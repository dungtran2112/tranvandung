﻿using System.Text;

string name = Console.ReadLine();
name= name.ToLower();
while(name.IndexOf("  ")!= -1)
{
    name= name.Replace("  ", " ");
}
StringBuilder fullName = new StringBuilder("");
string[] listWord = name.Split(" ");
for(int i = 0; i < listWord.Length; i++)
{
    string subName= listWord[i].Substring(0,1).ToUpper()+ listWord[i].Substring(1);
    fullName.Append(subName+" ");
}
Console.WriteLine(fullName);